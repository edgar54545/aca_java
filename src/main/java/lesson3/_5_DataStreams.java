package lesson3;

import java.io.*;

import static lesson3.Shared.*;

public class _5_DataStreams {
   public static void main(String[] args) throws IOException {
      deleteFile(filePath);

      try (DataOutputStream out = new DataOutputStream(new FileOutputStream(filePath))) {
         out.writeInt(10);
         out.writeChar('c');
         out.writeDouble(42.42);
         out.writeUTF(randomString());
      }

      System.out.println();

      try (DataInputStream in = new DataInputStream(new FileInputStream(filePath))) {
         System.out.println(in.readInt());
         System.out.println(in.readChar());
         System.out.println(in.readDouble());
         System.out.println(in.readUTF());
      }
   }
}

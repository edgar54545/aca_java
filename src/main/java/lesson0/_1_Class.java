package lesson0;

import java.util.ArrayList;
import java.util.List;

class Person {
   String name;
   long age;

   Person(String name, int age) {
      this.name = name;
      this.age = age;
   }

   boolean canDrink() {
      return age > 18;
   }
}

interface ICanDrive {
   void drive(String car);
}

class Driver extends Person implements ICanDrive {

   Driver(String name, int age) {
      super(name, age);
   }

   @Override
   public void drive(String car) {
      // todo
   }
}

class Student extends Person {

   Student(String name, int age) {
      super(name, age);
   }
}


public class _1_Class {
   public static void main(String[] args) {

      List<Person> persons = new ArrayList<>();

      persons.add(new Driver("joe", 42));
      persons.add(new Student("mary", 42));


      for (Person person : persons) {

            System.out.println(person.name + " " + person.getClass().getName());
      }


   }
}
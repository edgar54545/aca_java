package lesson0.binary;

public class BinaryUtil {

   public static String showBitsOf(long number) {
      long mask = 1L << 63;
      int count = 0;
      StringBuilder sb = new StringBuilder();

      while (mask != 0) {
         if ((number & mask) == 0) {
            sb.append('0');
         } else {
            sb.append('1');
         }

         if (count > 0 && count % 8 == 0) {
            sb.append(' ');
         }

         count++;
         mask = mask >>> 1;
      }

      return sb.toString();
   }

   public static String showBitsOf(int number) {
      int mask = 1 << 31;
      int count = 0;
      StringBuilder sb = new StringBuilder();

      while (mask != 0) {
         if ((number & mask) == 0) {
            sb.append('0');
         } else {
            sb.append('1');
         }

         if (count > 0 && count % 8 == 0) {
            sb.append(' ');
         }

         count++;
         mask = mask >>> 1;
      }

      return sb.toString();
   }
}
